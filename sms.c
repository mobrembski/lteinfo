#include "sms.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int startsWith2(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}

int structPutData(sms_entry_t *entry, char sectionNum, char *value, size_t valueSize)
{
  char *structMember = NULL;
    if (sectionNum > 5)
        return 1;
    if (value == 0)
        return 0;
    printf("structPutData %s size %d\n", value, valueSize);
    switch(sectionNum)
    {
        case 0:
            structMember = entry->id;
            break;
        case 1:
            structMember = entry->status;
            break;
        case 2:
            structMember = entry->receipient;
            break;
        case 3:
            structMember = entry->recv_date;
            break;
        case 4:
            structMember = entry->recv_time;
            break;
    }
    strncpy(structMember, value, valueSize);
    structMember[valueSize] = '\0';
    return 0;
}

int fillStructWithHeaderData(sms_entry_t *entry, char *headerLine)
{
  char *space, *tmpcopy, sectionsFound;
    sectionsFound = 0;
  size_t lengthOfInputString = strlen(headerLine);
  tmpcopy = malloc(sizeof(char) * lengthOfInputString + 1);
  strncpy(tmpcopy, headerLine, lengthOfInputString);
  tmpcopy[lengthOfInputString] = '\0';

  printf("fillStructWithHeaderData: %s\n", headerLine);
  space = strchr(tmpcopy, ' ');
  char *tok = strtok(space, ",") + 1; // Remove space from beginning
  while (tok != NULL)
  {
      size_t size;
      if (*tok == '\"')
          tok++;

      char *endingChar = strchr(tok, '\"');
      if (endingChar != NULL)
          size = endingChar - tok;
      else
          size = strlen(tok);

  if (structPutData(entry, sectionsFound++, tok, size))
  {
      printf("Trouble decoding line: %s\n", tok);
      free(tmpcopy);
      return 1;
  }
  entry->body[0] = '\0';

    tok = strtok(NULL, ",");
  }
  free(tmpcopy);
    return 0;
}

sms_entry_t * SMS_parseModemResponse(int *entriesCount, char *inputModemResponse)
{
  char * curLine = inputModemResponse;
  sms_entry_t *infoStruct = malloc(sizeof(struct sms_entry) * 120);
  int i;
  printf("ParseModemResponse input: %s", inputModemResponse);
  while(curLine)
  {
    char * nextLine = strchr(curLine, '\n');
    if (nextLine) *nextLine = '\0';  // temporarily terminate the current line
    if (startsWith2("+CMGL", curLine)) {
      fillStructWithHeaderData(&infoStruct[*entriesCount], curLine);
      (*entriesCount)++;
    } else {
      printf("ss: %d\n", strlen(curLine));
      for (i=0; i < strlen(curLine); i++) {
        if (curLine[i] < 32) {
          curLine[i] = ' ';
        }
      }
      strcat(infoStruct[*entriesCount - 1].body, curLine);
    }
    if (nextLine) *nextLine = '\n';  // then restore newline-char, just to be tidy
    curLine = nextLine ? (nextLine+1) : 0;
  }
  return infoStruct;
}

static void save_smsjs(FILE *f, sms_entry_t *readedSMS, int entriesCount)
{
  int i;
 fprintf(f, "\nmodemsms = [ ");
 for (i = 0; i < entriesCount; i++)
 {
   fprintf(f, "%s[\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\' ]",
        i ? "," : "",
        readedSMS[i].id,
        readedSMS[i].receipient,
        readedSMS[i].status,
        readedSMS[i].recv_time,
        readedSMS[i].recv_date,
        readedSMS[i].body);
 }
 fprintf(f, " ];\n");
}

void SMS_printToJSON(sms_entry_t *infoStruct, int entriesCount, char *output)
{
  FILE *f;

  if ((f = fopen("/var/spool/ltemodem-sms.js", "w")) != NULL) {
    save_smsjs(f, infoStruct, entriesCount);
    fclose(f);
    //rename("/var/tmp/rstats-history.js", "/var/spool/rstats-history.js");
  } else {
    printf("DUAP");
  }
}
