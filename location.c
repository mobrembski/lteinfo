#include "location.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <shutils.h>
#include <shared.h>

int startsWith2(const char *pre, const char *str);
/*{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}*/

static int structPutData(location_entry_t *entry, char sectionNum, char *value, size_t valueSize)
{
  int i;
    if (sectionNum >= 5)
        return 1;
    if (value == 0)
        return 0;
    char *structMember;
    switch(sectionNum)
    {
        case 0:
            structMember = entry->n;
            break;
        case 1:
            structMember = entry->status;
            break;
        case 2:
            structMember = entry->lac;
            break;
        case 3:
            structMember = entry->ci;
            break;
    }
    strncpy(structMember, value, valueSize);
    for (i=0; i < valueSize; i++) {
      if (structMember[i] < 32) {
        structMember[i] = ' ';
      }
    }
    structMember[valueSize] = '\0';
    return 0;
}

static int fillStructWithHeaderData(location_entry_t *entry, char *headerLine)
{
  char *space, *tmpcopy, sectionsFound;
  sectionsFound = 0;
  size_t lengthOfInputString = strlen(headerLine);
  tmpcopy = malloc(sizeof(char) * lengthOfInputString + 1);
  strncpy(tmpcopy, headerLine, lengthOfInputString);
  tmpcopy[lengthOfInputString] = '\0';

  space = strchr(tmpcopy, ' ');
  if (space == NULL) // in a case we don't have space separator
    space = strchr(tmpcopy,':');
  char *tok = strtok(space, ",") + 1; // Remove space from beginning
  while (tok != NULL)
  {
      size_t size;
      if (*tok == '\"')
          tok++;

      char *endingChar = strchr(tok, '\"');
      if (endingChar != NULL)
          size = endingChar - tok;
      else
          size = strlen(tok);
      tok[size] = '\0';

    if (structPutData(entry, sectionsFound++, tok, size))
    {
        printf("Trouble decoding line: %s\n", tok);
        free(tmpcopy);
        return 1;
    }
    tok = strtok(NULL, ",");
  }
  free(tmpcopy);
  return 0;
}

location_entry_t * Location_parseModemResponse(char *inputModemResponse)
{
  char * curLine = inputModemResponse;
  location_entry_t *infoStruct = NULL;
  while(curLine)
  {
    char * nextLine = strchr(curLine, '\n');
    if (nextLine) *nextLine = '\0';  // temporarily terminate the current line
    if (startsWith2("+CREG", curLine)) {
      infoStruct = malloc(sizeof(struct location_entry));
      fillStructWithHeaderData(infoStruct, curLine);
    }
    if (nextLine) *nextLine = '\n';  // then restore newline-char, just to be tidy
    curLine = nextLine ? (nextLine+1) : 0;
  }
  return infoStruct;
}

void Location_printToJSON(location_entry_t *entry, char *outputString)
{
  if ( sprintf(outputString, "\nmodemloc = [ \'%s\', \'%s\', \'%s\', \'%s\' ];\n",
             entry->n, entry->status, entry->lac, entry->ci) < 0 )
             {
               _dprintf("Error on passing location to JSON buffer");
               exit(1);
             }
  /*FILE *f;

  if ((f = fopen("/var/spool/ltemodem-location.js", "w")) != NULL) {
    fprintf(f, "\nmodemloc = [ \'%s\', \'%s\', \'%s\', \'%s\' ];\n",
               entry->n, entry->status, entry->lac, entry->ci);
    fclose(f);
    //rename("/var/tmp/rstats-history.js", "/var/spool/rstats-history.js");
  } else {
    printf("DUAP");
  }*/
}
