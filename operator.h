typedef struct operator_entry {
    char mode[2];
    char format[2];
    char name[64];
    char tech[2];
} operator_entry_t;
operator_entry_t * Operator_parseModemResponse(char *inputModemResponse);
void Operator_printToJSON(operator_entry_t *entry);
