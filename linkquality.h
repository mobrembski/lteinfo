#define MAXIMUM_HCSQ_FIELDS 5
typedef struct signal_info {
  char type[6];
  // Huawei docs says that there are 5 values from HCSQ,
  // but currently only 4 is used?
  int params[5];
} signal_info;

int Signal_parseModemResponse(signal_info *infoStruct, char *inputModemResponse);
void Signal_printToJSON(signal_info infoStruct, char *output);
