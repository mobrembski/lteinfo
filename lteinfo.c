/*

	rstats
	Copyright (C) 2006-2009 Jonathan Zarate


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <syslog.h>

#include <bcmnvram.h>
#include <shutils.h>


//#define DEBUG_NOISY
//#define DEBUG_STIME


#include <shared.h>

#include "common.h"

#include "linkquality.h"
#include "sms.h"
#include "operator.h"
#include "location.h"

int commandtorun = 0;
char errorOccured = 0;
char s[3024] = {};
char *modemDiagDev;
char cmdBuf[100];
FILE *f;

void saveStringToFile(const char *filename, char *inputString)
{
  FILE *f;
  if ((f = fopen(filename, "w")) != NULL) {
    fputs(inputString, f);
    fclose(f);
    printf("%s\n", inputString);
    //rename("/var/tmp/rstats-history.js", "/var/spool/rstats-history.js");
  } else {
    printf("DUAP");
  }
}

int startsWith(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}

int parseATCommand(FILE *f, char *totalChar, char *resultPrefix)
{
	size_t len = 0;
	char *line = NULL;
	ssize_t read;
	while ((read = getline(&line, &len, f)) != -1) {
		//Omit empty lines
		if (read > 2) {
			if (!strstr(line, "ERROR") && !(strstr(line, "COMMAND NOT SUPPORT"))) {
					if (strstr(line, "OK")) {
						break;
					}
					if (!resultPrefix) {
						strcat(totalChar,line);
					} else {
						if (startsWith(resultPrefix, line))
							strcat(totalChar,line);
					}
				}
			else {
				return 1;
			}
		}
	}
  if (strlen(totalChar) == 0) {
    return 1;
  }
	return 0;
}

void getLineState() {
	signal_info sigInfo;
  sprintf(cmdBuf, "MODE=\"AT^HCSQ?\" gcom -d %s -s /etc/gcom/setverbose.gcom", modemDiagDev);
	if ((f = popen(cmdBuf, "r")) == NULL) {
		perror("popen");
		return;
	}
	errorOccured = parseATCommand(f, s, "^HCSQ");
	pclose(f);

	if(errorOccured) {
		printf("An error occured %s\n", s);
		exit(1);
	}
	printf("%s", s);
	if (Signal_parseModemResponse(&sigInfo,s)) {
		printf("An error occured while parsing signal info\n");
	}
	Signal_printToJSON(sigInfo, s);
	printf("%s\n", s);
}

void getModemInfo() {
  FILE *f;
  int i;
  int nonFirstEntry=0;
  sprintf(cmdBuf, "gcom -d %s -s /etc/gcom/getcardinfo.gcom", modemDiagDev);
  if ((f = popen(cmdBuf, "r")) == NULL) {
    perror("popen");
    exit(1);
  }
  errorOccured = parseATCommand(f, s, NULL);
  pclose(f);

  if(errorOccured) {
    printf("An error occured %s\n", s);
    exit(1);
  }
  char * curLine = s;
  if ((f = fopen("/var/spool/ltemodem-info.js", "w")) == NULL) {
    printf("Cannot open file /var/spool/ltemodem-info.js for writing!\n");
    exit(1);
  }
  fprintf(f, "\nmodeminfo = [ ");
  while(curLine)
  {
    char * nextLine = strchr(curLine, '\n');
    if (nextLine) *nextLine = '\0';
    size_t lineLength = strlen(curLine);
    if(lineLength != 0)
    {
      for (i=0; i < lineLength; i++) {
        if (curLine[i] < 32) {
          curLine[i] = ' ';
        }
      }
      fprintf(f, "%s'%s'", nonFirstEntry++ == 0 ? "" : ",", curLine);
    }
    if (nextLine) *nextLine = '\n';
    curLine = nextLine ? (nextLine+1) : 0;
  }
  fprintf(f, " ];\n");
  fclose(f);
}

void getSMS() {
	sms_entry_t *readed_sms;
	int entryCount = 0;
  sprintf(cmdBuf, "gcom -d %s -s /tmp/getsms.gcom", modemDiagDev);
	if ((f = popen(cmdBuf, "r")) == NULL) {
		perror("popen");
		return;
	}
	errorOccured = parseATCommand(f, s, NULL);
	pclose(f);

	if(errorOccured) {
		printf("An error occured %s\n", s);
		exit(1);
	}
  printf("SMS_parseModemResponse %s\n", s);
	readed_sms = SMS_parseModemResponse(&entryCount,s);
	if (readed_sms < 0) {
		printf("An error occured while parsing signal info\n");
	}
	SMS_printToJSON(readed_sms, entryCount, s);
	free(readed_sms);
}

void getOperator()
{
  sprintf(cmdBuf, "gcom -d %s -s /tmp/getoperator.gcom", modemDiagDev);
  if ((f = popen(cmdBuf, "r")) == NULL) {
    perror("popen");
    return;
  }
  errorOccured = parseATCommand(f, s, "+COPS");
  pclose(f);

  if(errorOccured) {
    printf("An error occured %s\n", s);
    exit(1);
  }
  printf("Operator line : %s\n", s);
  operator_entry_t *entry = Operator_parseModemResponse(s);
  Operator_printToJSON(entry);
  free(entry);
}

void getLocation()
{
  char outputBuffer[1024];
  sprintf(cmdBuf, "gcom -d %s -s /tmp/getlocation.gcom", modemDiagDev);
  if ((f = popen(cmdBuf, "r")) == NULL) {
    perror("popen");
    return;
  }
  errorOccured = parseATCommand(f, s, "+CREG");
  pclose(f);

  if(errorOccured) {
    printf("An error occured %s\n", s);
    exit(1);
  }
  printf("Location line : %s\n", s);
  location_entry_t *entry = Location_parseModemResponse(s);
  Location_printToJSON(entry, outputBuffer);
  saveStringToFile("/var/spool/ltemodem-location.js", outputBuffer);
  free(entry);
}

static void findModemDiag()
{
  char *modemType = nvram_safe_get("modem_type");
  if (!strcmp(modemType, "hw-ether") || !strcmp(modemType, "non-hilink"))
  {
    dprintf("Modem with diagport detected.\n");
    modemDiagDev = nvram_safe_get("modem_dev4g");
    dprintf("Modem diagport: %s\n", modemDiagDev);
  }
  else {
    dprintf("[ERROR] Cannot find modem with diagport. Program cannot run.\n");
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  dprintf("Something");
  findModemDiag();
  if (argc<2) {
    printf("Usage: lteinfo --sms, --info, --signal, --operator or --location\n");
    exit(0);
  }
  if (!strcmp(argv[1],"--info"))
		getModemInfo();
  if (!strcmp(argv[1],"--sms"))
    getSMS();
  if (!strcmp(argv[1],"--signal"))
    getLineState();
  if (!strcmp(argv[1],"--operator"))
    getOperator();
  if (!strcmp(argv[1],"--location"))
    getLocation();

	return 0;
}
