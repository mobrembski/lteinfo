// Turn on Tomato built-in logging using cprintf.
// Please See Debug->Enable cprintf to /tmp/cprintf
#define DEBUG_NOISY
// Show messages also in console, not only in /tmp/cprintf
#define CONSOLE_NOISY

#ifdef CONSOLE_NOISY
#define dprintf(args...) printf(args); _dprintf(args);
#else
#define dprintf(args...) _dprintf(args);
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
//#include <shared.h>
#include <shutils.h>
