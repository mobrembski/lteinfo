include ../common.mak

CFLAGS	= -Os -Wall $(EXTRACFLAGS) #-mips32
CFLAGS	+= -I$(SRCBASE)/include -I$(TOP)/shared
LDFLAGS	=

ifeq ($(TCONFIG_BCMARM),y)
LIBS += -lgcc_s
endif

OBJS = lteinfo.o linkquality.o operator.o sms.o location.o

all: lteinfo

lteinfo: $(OBJS)
	@echo " [lteinfo] CC -o $@"
	@$(CC) $(LDFLAGS) -o $@ $(OBJS) -L../nvram${BCMEX} -lnvram -L../shared -lshared $(LIBS)

	$(SIZECHECK)
	$(CPTMP)

clean:
	rm -f lteinfo .*.depend
	rm -f *.o

install: all
	@echo " [lteinfo] Installing..."
	install -D lteinfo $(INSTALLDIR)/bin/lteinfo
	$(STRIP) $(INSTALLDIR)/bin/lteinfo
	chmod 0500 $(INSTALLDIR)/bin/lteinfo

%.o: %.c .%.depend
	@echo " [lteinfo] CC $@"
	@$(CC) $(CFLAGS) -c $<

.depend: $(OBJS:%.o=%.c)
	@$(CC) $(CFLAGS) -M $^ > .depend

.%.depend: %.c
	@$(CC) $(CFLAGS) -M $< > $@

-include $(OBJS:%.o=.%.depend)

