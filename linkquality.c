#include "linkquality.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int parseSpeed(signal_info *infoStruct, char *inputModemResponse)
{
  size_t i;
  // Copy whole repsonse, because strtok modify inputted string
  char tmp[300];
  strncpy(tmp,inputModemResponse,strlen(inputModemResponse));
  for (i=0; i<6; i++)
    infoStruct->type[i] = 0;
  strtok(tmp,"\"");
  char *endSpeed = strtok(NULL, "\"");
  strncpy(infoStruct->type, endSpeed, strlen(endSpeed));
  return 0;
}

void parseParameters(signal_info *infoStruct, char *inputModemResponse)
{
  size_t iter = 0;
  char *firstMember = strtok(inputModemResponse,",");
  while( (firstMember = strtok(NULL, ",")) )
  {
    infoStruct->params[iter++] = atoi(firstMember);
  }
  // Fill unused fields with -1
  while (iter++ < MAXIMUM_HCSQ_FIELDS) { infoStruct->params[iter] = -1; }
}

int Signal_parseModemResponse(signal_info *infoStruct, char *inputModemResponse)
{
  if (parseSpeed(infoStruct, inputModemResponse))
    return 1;
  parseParameters(infoStruct, inputModemResponse);
  return 0;
}

void Signal_printToJSON(signal_info infoStruct, char *output)
{
  FILE *f;

  if ((f = fopen("/var/spool/ltemodem-signal.js", "w")) != NULL) {
    fprintf(f, "\nmodemsignal = [ \'%s\', \'%d\', \'%d\', \'%d\', \'%d\', \'%d\' ];\n",
    infoStruct.type, infoStruct.params[0],infoStruct.params[1],infoStruct.params[2],infoStruct.params[3],infoStruct.params[4]);
    fclose(f);
    //rename("/var/tmp/rstats-history.js", "/var/spool/rstats-history.js");
  } else {
    printf("An error occure");
  }
}
