typedef struct location_entry {
    char n[2];
    char status[2];
    char lac[6];
    char ci[9];
} location_entry_t;
location_entry_t * Location_parseModemResponse(char *inputModemResponse);
void Location_printToJSON(location_entry_t *entry, char *outputString);
