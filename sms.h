typedef struct sms_entry {
    char id[3];
    char receipient[255];
    char status[12];
    char recv_time[13];
    char recv_date[9];
    char body[255];
} sms_entry_t;

sms_entry_t * SMS_parseModemResponse(int *entriesCount, char *inputModemResponse);
void SMS_printToJSON(sms_entry_t *infoStruct, int entriesCount, char *output);
